import asyncio
import json
import logging
import multiprocessing
from random import random, randint
import time
import uuid

import zmq

from logistics import HubManager


JOB_QUEUE_ADDRESS = "tcp://localhost:5557"
RESULT_QUEUE_ADDRESS = "tcp://127.0.0.1:5558"


def spawn_jobs(N):

    # Create job queue socket
    ctx = zmq.asyncio.Context()
    job_socket = ctx.socket(zmq.PUSH)
    job_socket.connect(JOB_QUEUE_ADDRESS)

    logging.debug("Queueing {} jobs in 1 sec...".format(N))
    time.sleep(1)
    for i in range(N):
        new_job = {
            "uuid": str(uuid.uuid4()),
            "destination": 1e3 * (1 + random()),
            "region": randint(0, 4)
        }
        logging.debug("Client sending {}".format(json.dumps(new_job)))
        job_socket.send(bytes("{}".format(json.dumps(new_job)).encode("UTF-8")))
    logging.debug("Done queueing new jobs")

    job_socket.close()


async def collect_results(num_results, loop):

    logging.debug("Started collect_results")

    # Create result queue socket
    ctx = zmq.asyncio.Context()
    res_socket = ctx.socket(zmq.PULL)
    res_socket.bind(RESULT_QUEUE_ADDRESS)

    for i in range(num_results):
        await res_socket.recv()

    logging.debug("Got all {} results!".format(num_results))
    tasks = [task for task in asyncio.Task.all_tasks(loop=loop) if task is not asyncio.Task.current_task(loop=loop)]
    for task in tasks:
        task.cancel()

    res_socket.close()

    return


def NxN_test(num_trucks, num_drivers, num_jobs=10):

    logging.debug("Number of Trucks: {}".format(num_trucks))
    logging.debug("Number of Drivers: {}".format(num_drivers))

    # Create new event loop in new thread to run server
    manager = HubManager(num_trucks, num_drivers, [])

    # Add any additional coroutines to the runtime we want
    manager.user_coroutines.append((collect_results, (num_jobs, manager.loop,)))

    # Start the event loop thread
    manager.start()

    # Give the dispatcher some jobs
    p = multiprocessing.Process(target=spawn_jobs, args=(num_jobs,))
    p.start()
    p.join()

    # Join the thread when done
    manager.join()


"""
def test_1x1():
    NxN_test(1, 1)
    assert True


def test_2x1():
    NxN_test(2, 1)
    assert True


def test_1x2():
    NxN_test(1, 2)
    assert True


def test_2x2():
    NxN_test(2, 2)
    assert True


def test_4x2():
    NxN_test(4, 2)
    assert True


def test_2x4():
    NxN_test(2, 4)
    assert True
"""


def test_4x4():
    NxN_test(4, 4, 60)
    assert True
