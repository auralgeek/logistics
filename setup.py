from setuptools import setup, find_packages

setup(name='logistics',
      version='0.1.0',
      description='Job based hardware manager',
      packages=find_packages(),
      zip_safe=False)
