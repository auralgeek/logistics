Logistics
=========

Goal for this is to be a job based asynchronous hardware manager.  Uses modern
python concepts like asyncio to make asynchronous calls to hardware that can
use callbacks to notify the caller that the job is done and results are ready.

Input jobs received handled via input queue.

Run tests with:

``` sh
$ pytest --log-cli-level=10 --durations=0 .
```

to monitor all logging output.
