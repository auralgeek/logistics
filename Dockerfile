FROM python:3.6-slim

WORKDIR /app

ADD . /app

RUN yes | pip install -r requirements.txt

EXPOSE 5557

CMD ["pytest", "--log-cli-level=10", "--duration=0", "."]
