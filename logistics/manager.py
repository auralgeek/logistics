import asyncio
import json
import logging
from random import shuffle, random, randint
from threading import Thread
import sys
import time

import zmq

from .resources import Hub


INPUT_QUEUE_ADDRESS = "tcp://127.0.0.1:5557"
OUTPUT_QUEUE_ADDRESS = "tcp://localhost:5558"
logging.basicConfig(level=logging.DEBUG)


class HubManager(object):

    def __init__(self, num_trucks, num_drivers, user_coroutines=None):
        self.loop = asyncio.new_event_loop()
        self.user_coroutines = user_coroutines

        self.hub = Hub(num_trucks, num_drivers, self.loop)

    def start_server_loop(self, loop, hub, user_coroutines=None):
        asyncio.set_event_loop(loop)
        task_list = [asyncio.ensure_future(dispatcher(hub, loop), loop=loop)]
        for c, args in user_coroutines:
            task_list.append(asyncio.ensure_future(c(*args), loop=loop))

        loop.run_until_complete(
            asyncio.gather(*task_list, loop=loop)
        )

    def start(self):
        logging.debug("Starting HubManager thread...")
        logging.debug(self.user_coroutines)
        self.thread = Thread(target=self.start_server_loop,
            args=(self.loop, self.hub, self.user_coroutines))
        self.thread.start()

    def join(self):
        self.thread.join()
        logging.debug("HubManager thread joined!")


class JobRequest(object):

    def __init__(self, uuid, destination, region):
        self.uuid = uuid
        self.destination = destination
        self.region = region


class ResultPackage(object):

    def __init__(self, uuid, package):
        self.uuid = uuid
        self.package = package


async def dispatcher(hub, loop):

    in_socket = hub.zmq_context.socket(zmq.PULL)
    in_socket.bind(INPUT_QUEUE_ADDRESS)

    while True:
        try:
            logging.debug("Pulling a job from the input queue...")
            job_msg = await in_socket.recv()
            job_dict = json.loads(job_msg)
            logging.debug("Dispatcher got job {}".format(job_dict))
            hub.in_queue.put_nowait(JobRequest(
                job_dict["uuid"],
                job_dict["destination"],
                job_dict["region"]
            ))
            asyncio.ensure_future(worker_lifetime(job_dict["uuid"], hub, loop), loop=loop)

            await asyncio.sleep(0.5, loop=loop)

        except asyncio.CancelledError:
            logging.debug("Dispatcher cancelled!")
            in_socket.close()
            break


async def worker_lifetime(worker_id, hub, loop):

    # Get next job from hub.in_queue
    job_in = await hub.in_queue.get()
    logging.debug("{}: Got job {}".format(worker_id, job_in))
    logging.debug("{}: Job region: {}, destination: {}".format(worker_id, job_in.region, job_in.destination))

    logging.debug("{}: Submitting job request to hub...".format(worker_id))
    package_future = asyncio.Future(loop=loop)
    await hub.execute_job(job_in.region, job_in.destination, delivery_job,
        (worker_id, package_future, loop))

    logging.debug("{}: Processing new package: {}".format(worker_id, package_future.result()))
    result_package = ResultPackage(job_in.uuid, package_future.result())
    asyncio.ensure_future(process_delivery(hub.zmq_context, result_package), loop=loop)

    return


async def delivery_job(worker_id, future, loop):

    # TODO: Do some slightly more 'real' asynchronous work here
    logging.debug("{}: Driving...".format(worker_id))
    await asyncio.sleep(0.5 * random(), loop=loop)
    logging.debug("{}: Delivering...".format(worker_id))
    await asyncio.sleep(0.5 * random(), loop=loop)
    logging.debug("{}: Returning...".format(worker_id))
    await asyncio.sleep(0.5 * random(), loop=loop)

    val = random()
    try:
        future.set_result(val)
    except asyncio.InvalidStateError as e:
        logging.error("{}: {}".format(worker_id, e))
        logging.error("{}: {}".format(worker_id, val))

    return


async def process_delivery(context, result_package):

    logging.debug("Processing package...")
    out_socket = context.socket(zmq.PUSH)
    out_socket.connect(OUTPUT_QUEUE_ADDRESS)
    out_socket.send(bytes(json.dumps({
        "uuid": result_package.uuid,
        "package": result_package.package
    }).encode("UTF-8")))
    logging.debug("Package processed!")

    return
