import asyncio

import logging
from random import shuffle, randint

import zmq
import zmq.asyncio


class Driver(object):

    def __init__(self, driver_id, destination, loop):
        self.id = driver_id
        self.destination = destination
        self.loop = loop
        self.lock = asyncio.Lock(loop=loop)

    async def set_destination(self, destination):
        if self.destination != destination:
            self.destination = destination
            await asyncio.sleep(4, loop=self.loop)

    async def do_job(self, destination, job, args=()):
        async with self.lock:
            await self.set_destination(destination)
            await job(*args)


RANGE = 50.0
class TruckRegion(object):

    def __init__(self, region, center, loop):
        self._set(region, center)
        self.loop = loop

    def _set(self, region, center):
        self.region = region
        self.center = center
        self.min = center - RANGE
        self.max = center + RANGE

    async def set(self, region, center):
        self._set(region, center)
        await asyncio.sleep(0.1, self.loop)

    def in_range(self, destination):
        if destination > self.min and destination < self.max:
            return True
        else:
            return False

    def check_valid(self, region, destination):
        if self.in_range(destination) and self.region == region:
            return True
        else:
            return False


class Truck(object):

    def __init__(self, truck_id, region, center, num_drivers, loop):
        self.id = truck_id
        self.region = TruckRegion(region, center, loop)
        self.num_drivers = num_drivers
        self.loop = loop

        self.drivers = {}
        for i in range(num_drivers):
            self.drivers[i] = Driver(i, None, self.loop)
        self.semaphore = asyncio.Semaphore(num_drivers, loop=self.loop)
        self.interface = asyncio.Lock(loop=self.loop)


    async def do_job(self, region, destination, job, args=()):
        async with self.semaphore:
            while True:
                if self.region.check_valid(region, destination):

                    # Already set to a region with coverage for this job
                    logging.debug("Picking present driver...")
                    for did, driver in self.drivers.items():
                        if driver.destination == destination:
                            await self.drivers[did].do_job(destination, job, args=args)
                            return

                    # If no driver at destination already, then pick one available
                    logging.debug("Picking available driver...")
                    for did, driver in self.drivers.items():
                        if not driver.lock.locked():
                            await self.drivers[did].do_job(destination, job, args=args)
                            return

                    # If no driver available, then just pick one randomly and wait
                    logging.debug("Picking random driver...")
                    id_list = [driver.id for _, driver in self.drivers.items()]
                    shuffle(id_list)
                    did = id_list[0]
                    await self.drivers[did].do_job(destination, job, args=args)
                    return

                elif self.all_drivers_free():
                    # Available for reallocating region to cover job
                    logging.debug("Reallocating Truck...")
                    await self.region.set(region, destination)
                    id_list = [driver.id for _, driver in self.drivers.items()]
                    shuffle(id_list)
                    did = id_list[0]
                    await self.drivers[did].do_job(destination, job, args=args)
                    return

                await asyncio.sleep(0.1, loop=self.loop)

    def all_drivers_free(self):
        if not any([driver.lock.locked() for _, driver in self.drivers.items()]):
            return True
        else:
            return False


class Hub(object):

    def __init__(self, num_trucks, num_drivers, loop):
        self.num_trucks = num_trucks
        self.num_drivers = num_drivers
        self.loop = loop

        self.trucks = {}
        for i in range(num_trucks):
            self.trucks[i] = Truck(i, 0, 0, num_drivers, self.loop)
        self.zmq_context = zmq.asyncio.Context()
        self.in_queue = asyncio.Queue(loop=self.loop)

    async def execute_job(self, region, destination, job, args=()):

        # Block until a truck meets criteria and has a driver available
        while True:

            # If a Truck is already operating in a region, select it for the job
            for tid in self.trucks:
                if self.trucks[tid].region.check_valid(region, destination):
                    logging.debug("Truck picked based on presence")
                    await self.trucks[tid].do_job(region, destination, job, args=args)
                    return

            # If a Truck is available for reallocation, select it
            for tid in self.trucks:
                if self.trucks[tid].all_drivers_free():
                    logging.debug("Truck picked based on allocation")
                    await self.trucks[tid].do_job(region, destination, job, args=args)
                    return

            await asyncio.sleep(0.1, loop=self.loop)
